var config			= require('../config/database'),
redis 			= require('redis'),
redisClient = redis.createClient(),
User				= require('../models/user');

var TOKEN_EXPIRATION = 1;
var TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION * 60;

module.exports.login = (req, res) => {
	var username = req.body.username || '';
	var password = req.body.password || '';

	if (username == '' || password == '') {
		return res.status(401).send({success: false, msg: "Undefined"});
	}

	if(User.username === req.body.username && User.password === req.body.password) {
		req.jwtSession.User = {
			username : User.username,
			firstname: User.firstname,
			lastname : User.lastname,
			role		 : User.role
		}

		var claims = {
			iss: "my application name",
			aud: "myapplication.com",
			username : User.username,
		}

		req.jwtSession.create(claims, function(error, token){
			return res.json({ token: token });
			console.log(token)
		})
	}

}

module.exports.logout = (req, res, next) => {
	if (req.jwtSession.User) {
		var token = req.headers['x-jwt-token'];
		if(token != null || token == req.jwtSession.jwt) {
//      redisClient.expire(req.jwtSession.user, TOKEN_EXPIRATION_SEC);
//			return res.send({success: true, msg: 'Logging out...'});

			//destroy session token from storage
			req.jwtSession.destroy(function(err){
				if(err) throw err

					res.status(200).send({});
				next()
			});
		}
	} else {
		return res.status(401).send({success: true, msg: 'TokenMalformed found...'});
	}
}
