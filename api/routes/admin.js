
const User   = require('../models/user')


module.exports.dashboard = (req, res) => {
	var name = req.jwtSession.User.firstname + ' ' + req.jwtSession.User.lastname
	const data = {
		name : name,
		role : req.jwtSession.user.role
	}
	res.json(data)
}

