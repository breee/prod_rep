'use strict'

var express        = require('express'),
		redis          = require('./config/database'),
		JWTRedisSession= require('jwt-redis-session'),
		bodyParser     = require('body-parser'),
		logger         = require('morgan'),
		path           = require('path'),
		config         = require('./config/database'),
		User           = require('./models/user'),
		jwt            = require('express-jwt'),
		authToken      = require('./config/auth'),
 		fse 					 = require('fs-extra'),
		app            = express(),
		jwtRouter      = express.Router();


var fs = require('fs-extra')
var pub = fs.readFileSync(path.join(__dirname, 'pub.pem'));
var priv = fs.readFileSync(path.join(__dirname, 'priv.pem'));

//static path config
app.use(express.static(path.join(__dirname, '../app')))

//logger config
app.use(logger('dev'))

//bodyparser config
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//JWT redis session config
app.use(JWTRedisSession({
	client    : redis.client,
	passphrase: '137BAB574D',
	pubkey    : pub,
	secret    : priv,
	keyspace  : "sess:",
	maxAge    : 1800,
  algorithm : "RS256",
  requestKey: "jwtSession",
  requestArg: "jwtToken"
}))

app.all('*', function(req, res, next) {
	res.set('Access-Control-Allow-Origin', 'http://localhost');
	res.set('Access-Control-Allow-Credentials', true);
	res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
	res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, x-jwt-token');
	if ('OPTIONS' == req.method) return res.sendStatus(200);
	next();
});

//Routes
var routes = {};
routes.users = require('./routes/users')
routes.admin = require('./routes/admin')

// app.post('/signup/', function(req, res) {
// 	if (!req.body.username) {
// 		res.json({success: false, msg: 'Please pass name.'});
// 	} else {
// 		  var username  = req.body.username,
//       password  = req.body.password,
//       firstname = req.body.firstname,
//       lastname  = req.body.lastname,
//       role      = req.body.role;

//     var newUser = new User()
//       newUser.username   = username
//       newUser.password   = password
//       newUser.firstname  = firstname
//       newUser.lastname   = lastname
//       newUser.role		   = role


// 		// save the user
// 		newUser.save(function(err) {
// 			if (err) throw err;
// 			res.json({success: true, msg: 'Successful created new member.'});
// 		});
// 	}
// })

//Route auth middleware
jwtRouter.post('/authenticate', routes.users.login, jwt({secret: pub}))

//Admin route
jwtRouter.get('/', authToken.verifyToken, routes.admin.dashboard)

//Logout route
jwtRouter.get('/logout', routes.users.logout)

//Routes
app.use('/admin', jwtRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found')
	err.status = 404
	next(err)
})

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
		app.use(function(err, req, res, next) {
				res.status(err.status || 500);
				res.render('error', {
						message: err.message,
						error: err
				});
		});

		app.use(function (err, req, res, next) {
			if (err.name === 'UnauthorizedError') {
				res.send(401, 'invalid token...');
			}
		});
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
				message: err.message,
				error: {}
		});
})

module.exports = app
